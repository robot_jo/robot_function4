*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}         http://127.0.0.1:8080/manageStudentConsider
${web_browser}      chrome

${txt_search}       searchuser
${name}             ชรัญธร

${btn_search}       btnsearch
${btn_add}          btnAdd
${btn_edit}         xpath=//a[@href="/manageCon_edit/1"]
${btn_delete}       xpath=//a[@href="/delCon/1"]

***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กรอกค้นหา
    [Arguments]             ${txt}
    [Documentation]         ใช้ในการค้นหาข้อมูล
    input text              ${txt_search}       ${txt} 

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้สำหรับการกดปุ่ม
    click button            ${btn}

กดลิ้ง
    [Arguments]             ${link}
    [Documentation]         ใช้สำหรับการกด link
    click link              ${link}

***Test Cases***
TS-PSF-03-02-01 ตรวจสอบการกดปุ่มค้นหา
    เปิดหน้าจอเว็บ             
    กรอกค้นหา                ${name}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-02-02 ตรวจสอบการกดปุ่มเพิ่มข้อมูล
    เปิดหน้าจอเว็บ            
    กดปุ่ม                    ${btn_add}

TS-PSF-03-02-03 ตรวจสอบการกดปุ่มแก้ไข
    เปิดหน้าจอเว็บ            
    กดลิ้ง                    ${btn_edit}

TS-PSF-03-02-04 ตรวจสอบการกดปุ่มลบ
    เปิดหน้าจอเว็บ             
    กดลิ้ง                    ${btn_delete}