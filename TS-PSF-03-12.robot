*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentConsider
${web_browser}              chrome
***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}
***Test Cases***
TS-PSF-03-12-01 ตรวจสอบการแสดงผลของหน้านิสิตรอการพินิจหลังทำการกดปุ่มตกลงในหน้าลบข้อมูล
    เปิดหน้าจอเว็บ
    

TS-PSF-03-13-02 ตรวจสอบการแสดงผลของหน้านิสิตรอการพินิจหลังทำการกดปุ่มยกเลิกในหน้าลบข้อมูล
    เปิดหน้าจอเว็บ