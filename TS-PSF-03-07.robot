*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                     http://127.0.0.1:8080/manageCon_edit/1
${web_browser}                  chrome

${txt_ID}                       studentIDCon
${txt_name}                     nameStuCon
${txt_level}                    stuLevelCon
${txt_grade}                    stuGpaCon
${txt_status}                   stuStatusCon
${txt_creditPass}               creditPassedCon
${txt_creditDown}               creditDownCon

${user_ID1}                     58660055
${user_ID2}                     564
${user_ID3}                     5/*-uyjL
${name1}                        ชรัญญา วงษ์ไทย
${name2}                        อิ๊ง วรัญธร
${name3}                        48@&!+-*++##
${grade1}                       9.99
${grade2}                       2.4
${grade3}                       าาม
${creditPass1}                  156
${creditPass2}                  1460
${creditPass3}                  +!#
${creditDown1}                  -150
${creditDown2}                  1500
${creditDown3}                  %&&

${btn_summit}                   ok

***Keywords***
เปิดลิ้งค์
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt}      ${input}
    [Documentation]         ใช้สำหรับกรอกข้อมูล
    clear element text      ${txt}
    input text              ${txt}      ${input}

ลบข้อมูล
    [Arguments]             ${txt}
    [Documentation]         ใช้ลบข้อมูลใน element
    clear element text      ${txt}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้กดปุ่ม
    click button            ${btn}

***Test Cases***
TS-PSF-03-07-01 ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_ID}     ${user_ID1}

TS-PSF-03-07-02 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_name}             ${name1}

TS-PSF-03-07-03 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_grade}            ${grade1}

TS-PSF-03-07-04 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditPass}       ${creditPass1}

TS-PSF-03-07-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditDown}       ${creditDown1}

TC-PSF-03-07-06 ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร และไม่น้อยกว่า 8 ตัวอักษร
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_ID}               ${user_ID2}

TC-PSF-03-07-07 ตรวจสอบการกรอกข้อมูลชื่อ - นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_name}             ${name2}

TC-PSF-03-07-08 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย รับทศนิยมไม่เกิน 2 หลัก และทศนิยมไม่น้อยกว่า 2 หลัก
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_grade}            ${grade2}

TC-PSF-03-07-09 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน รับเลขไม่เกิน 3 หลัก
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditPass}       ${creditPass2}

TC-PSF-03-07-10 ตรวจสอบการกรอกข้อมูลหน่วยกิตลง รับเลขไม่เกิน 3 หลัก
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditDown}       ${creditDown2}

TC-PSF-03-07-11 ตรวจสอบการกรอกหากไม่กรอกข้อมูลรหัสนิสิต
    เปิดลิ้งค์
    ลบข้อมูล                  ${txt_ID}
    กดปุ่ม                    ${btn_summit}

TC-PSF-03-07-12 ตรวจสอบการกรอกหากไม่กรอกข้อมูลชื่อ - นามสกุล
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_ID}               ${user_ID1}
    ลบข้อมูล                  ${txt_name}
    กดปุ่ม                    ${btn_summit}

TC-PSF-03-07-13 ตรวจสอบการกรอกหากไม่กรอกข้อมูลเกรดเฉลี่ย
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_name}             ${name1}
    ลบข้อมูล                  ${txt_grade}
    กดปุ่ม                    ${btn_summit}

TC-PSF-03-07-14 ตรวจสอบการกรอกหากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดลิ้งค์
    กรอกข้อมูล                 ${txt_grade}            ${grade1}
    ลบข้อมูล                  ${txt_creditPass}
    กดปุ่ม                    ${btn_summit}

TC-PSF-03-07-15 ตรวจสอบการกรอกหากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิดลิ้งค์
    กรอกข้อมูล                 ${txt_creditPass}       ${creditPass1}
    ลบข้อมูล                  ${txt_creditDown}
    กดปุ่ม                    ${btn_summit}

TC-PSF-03-07-16 ตรวจสอบการกรอกข้อมูลรหัสนิสิตเป็นตัวอักษรมั่ว
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_ID}               ${user_ID3}

TC-PSF-03-07-17 ตรวจสอบการกรอกข้อมูลชื่อ - นามสกุลเป็นตัวอักษรมั่ว
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_name}             ${name3}

TC-PSF-03-07-18 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ยเป็นตัวอักษรมั่ว
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_grade}            ${grade3}

TC-PSF-03-07-19 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่านเป็นตัวอักษรมั่ว
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditPass}       ${creditPass3}

TC-PSF-03-07-20 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลงเป็นตัวอักษรมั่ว
    เปิดลิ้งค์
    กรอกข้อมูล                ${txt_creditDown}       ${creditDown3}

    