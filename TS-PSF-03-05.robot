*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentCon_add
${web_browser}              chrome
${txt_ID}                   studentIDCon
${txt_name}                 nameStuCon
${txt_level}                stuLevelCon
${txt_grade}                stuGpaCon
${txt_status}               stuStatusCon
${txt_creditPass}           creditPassedCon
${txt_creditDown}           creditDownCon
${txt_conCon}               considerCon
${txt_conTime}              considertime
 
${user_ID}                  64160000
${name}                     จิรายุ เจริญผลวัฒนา
${level}                    ตรี ปกติ
${grade}                    1.95
${status}                   10
${creditPass}               90
${creditDown}               100
${con}                      โปรต่ำ
${time}                     1

${btn_summit}               ok
${btn_cancel}               cancel

***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}
กรอกข้อมูล
    [Arguments]             ${txt_name}         ${txt}
    [Documentation]         ใช้สำหรับการกรอกข้อมูล
    input text              ${txt_name}         ${txt}
กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้สำหรับกดปุ่ม
    click button            ${btn}


กรอกข้อมูลทั้งหมด
    [Arguments]         ${ID}       ${name}         ${level}            ${grade}        ${status}       ${creditPass}       ${creditDown}        ${con}      ${time}
    [Documentation]     ใช้กรอกข้อมูลทั้งหมด   
    input text                      ${txt_id}               ${ID}
    input text                      ${txt_name}             ${name}
    select from list by label       ${txt_level}            ${level}
    input text                      ${txt_grade}            ${grade}
    select from list by label       ${txt_status}           ${status}
    input text                      ${txt_creditPass}       ${creditPass}
    input text                      ${txt_creditDown}       ${creditDown}
    select from list by label       ${txt_conCon}           ${con}
    select from list by label       ${txt_conTime}          ${time}
***Test Cases***
TS-PSF-03-05-01 ตรวจสอบการกดปุ่มบันทึก  
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด                   ${user_ID}      ${name}         ${level}        ${grade}    ${status}       ${creditPass}       ${creditDown}        ${con}          ${time}
    กดปุ่ม                            ${btn_summit}

TS-PSF-03-05-02 ตรวจสอบการกดปุ่มยกเลิก
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด                   ${user_ID}      ${name}         ${level}        ${grade}    ${status}       ${creditPass}       ${creditDown}        ${con}          ${time}
    กดปุ่ม                            ${btn_cancel}