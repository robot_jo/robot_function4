*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentConsider
${web_browser}              chrome

${txt_name}                 nameStuCon

${name}                     ธนัท กาญจนมาศ

${btn_edit}                 xpath=//a[@href="/manageCon_edit/1"]
${btn_summit}               ok
${btn_cancel}               cancel


***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กดลิ้ง
    [Documentation]         ใช้สำหรับการกด link
    click link              ${btn_edit}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้สำหรับการกดปุ่ม
    click button            ${btn}

กรอกข้อมูล
    [Arguments]             ${name}
    [Documentation]         ใช้สำหรับการกดปุ่ม
    input text              ${txt_name}         ${name}
    
***Test Cases***
TS-PSF-03-10-01 ตรวจสอบการทำงานของปุ่มบันทึก
    เปิดหน้าจอเว็บ
    กดลิ้ง
    กรอกข้อมูล                ${name}
    กดปุ่ม                    ${btn_summit} 

TS-PSF-03-10-02 ตรวจสอบการทำงานของปุ่มยกเลิก
    เปิดหน้าจอเว็บ
    กดลิ้ง
    กรอกข้อมูล                ${name}
    กดปุ่ม                    ${btn_cancel}   