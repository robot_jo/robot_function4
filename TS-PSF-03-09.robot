*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentCon_add
${web_browser}              chrome
***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}
***Test Cases***
TC-PSF-03-09-01 ตรวจสอบการแสดงผลข้อมูลรหัสนิสิต
    เปิดหน้าจอเว็บ 

TC-PSF-03-09-02 ตรวจสอบการแสดงผลข้อมูลชื่อ - นามสกุล
    เปิดหน้าจอเว็บ

TC-PSF-03-09-03 ตรวจสอบการแสดงผลข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอเว็บ

TC-PSF-03-09-04 ตรวจสอบการแสดงผลข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอเว็บ

TC-PSF-03-09-05 ตรวจสอบการแสดงข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอเว็บ