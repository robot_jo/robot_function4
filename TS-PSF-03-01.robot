*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser
***Variables***
${URL_page}         http://127.0.0.1:8080/manageStudentConsider
${web_browser}      chrome

&{txt_search}       searchuser

${name}             ชรัญธร
${surname}          วงษ์ไทย
${ID}               58660044
${name_surname}     ชรัญธร วงษ์ไทย
${full_name}        ชรัญธร วงษ์ไทย 58660044
${txt50}            0123456789012345678901234567890123456789012345678901234657980123456789

${btn_search}       btnsearch

***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กรอกค้นหา
    [Arguments]             ${txt}
    [Documentation]         ใช้ในการค้นหาข้อมูล
    input text              &{txt_search}       ${txt} 

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้สำหรับการกดปุ่ม
    click button            ${btn}
*** Test Cases ***
TS-PSF-03-01-01 ตรวจสอบการกรอกข้อมูลค้นจากชื่อ
    เปิดหน้าจอเว็บ            
    กรอกค้นหา                ${name}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-02 ตรวจสอบการกรอกข้อมูลค้นหาจากการใส่เพียงนามสกุล
    เปิดหน้าจอเว็บ           
    กรอกค้นหา                ${surname}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-03 ตรวจสอบการค้นหาจากการใส่เพียง รหัสนิสิต
    เปิดหน้าจอเว็บ            
    กรอกค้นหา                ${ID}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-04 ตรวจสอบการกรอกข้อมูลค้นหาจาก ชื่อ-นามสกุล
    เปิดหน้าจอเว็บ             
    กรอกค้นหา                ${name_surname}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-05 ตรวจสอบการกรอกข้อมูลค้นหาจาก ชื่อ-นามสกุล รหัสนิสิต
    เปิดหน้าจอเว็บ       
    กรอกค้นหา                ${full_name}
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-06 ตรวจสอบการกรอกข้อมูลค้นหาโดยไม่กรอกอะไรเลย
    เปิดหน้าจอเว็บ             
    กดปุ่ม                    ${btn_search}

TS-PSF-03-01-07 ตรวจสอบการกรอกข้อมูลค้นหา รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอเว็บ            
    กรอกค้นหา                ${txt50}
    กดปุ่ม                    ${btn_search}
