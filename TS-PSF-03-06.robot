*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentCon_add
${web_browser}              chrome

${txt_level}                stuLevelCon
${txt_status}               stuStatusCon
${txt_conCon}               considerCon
${txt_conTime}              considertime

${level1}                           0
${level2}                           1
${level3}                           2
${level4}                           3
***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กดปุ่ม
    [Arguments]             ${btn}
    [Documentation]         ใช้สำหรับกดปุ่ม
    click button            ${btn}

เช็ค Drop down 
    [Arguments]             ${txt}
    [Documentation]         เช็ค List
    page should contain list        ${txt}
    select from list by index       ${txt}      ${level2}

เลือก Drop down
    [Arguments]                     ${txt}      ${level}
    [Documentation]                 ใช้ เลือก Dropdown
    select from list by index       ${txt}      ${level}

***Test Cases***
TS-PSF-03-06-01 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลระดับ
    เปิดหน้าจอเว็บ             
    เช็ค Drop down           ${txt_level}

TS-PSF-03-06-02 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลสถานภาพ
    เปิดหน้าจอเว็บ             
    เช็ค Drop down           ${txt_status}

TS-PSF-03-06-03 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลรอการพิจารณา
    เปิดหน้าจอเว็บ             
    เช็ค Drop down           ${txt_conCon}

TS-PSF-03-06-04 ตรวจสอบการแสดงผลฟิลเตอร์ Dropdown ข้อมูลครั้งที่
    เปิดหน้าจอเว็บ             
    เช็ค Drop down           ${txt_conTime}


TS-PSF-03-06-05 ตรวจสอบการแก้ไข Drop down list ระดับ กรณีตัวเลือก “ตรี พิเศษ”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_level}            ${level1} 

TS-PSF-03-06-06 ตรวจสอบการแก้ไข Drop down list ระดับ กรณีตัวเลือก “ตรี ปกติ”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_level}            ${level2} 

TS-PSF-03-06-07 ตรวจสอบการแก้ไข Drop down list สถานะภาพ กรณีตัวเลือก “10”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_status}           ${level1} 

TS-PSF-03-06-08 ตรวจสอบการแก้ไข Drop down list สถานะภาพ กรณีตัวเลือก “12”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_status}           ${level2} 

TS-PSF-03-06-09 ตรวจสอบการแก้ไข Drop down list รอการพิจารณา กรณีตัวเลือก “โปรต่ำ”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conCon}           ${level1} 
 	
TS-PSF-03-06-10 ตรวจสอบการแก้ไข Drop down list รอการพิจารณา กรณีตัวเลือก “โปรสูง”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conCon}           ${level2} 

TS-PSF-03-06-11 ตรวจสอบการแก้ไข Drop down list ครั้งที่ กรณีตัวเลือก “1”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conTime}          ${level1}

TS-PSF-03-06-12 ตรวจสอบการแก้ไข Drop down list ครั้งที่ กรณีตัวเลือก “2”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conTime}          ${level2}

TS-PSF-03-06-13 ตรวจสอบการแก้ไข Drop down list ครั้งที่ กรณีตัวเลือก “3”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conTime}          ${level3}

TS-PSF-03-06-14 ตรวจสอบการแก้ไข Drop down list ครั้งที่ กรณีตัวเลือก “4”
    เปิดหน้าจอเว็บ
    เลือก Drop down          ${txt_conTime}          ${level4}