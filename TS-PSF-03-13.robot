*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 hhttp://127.0.0.1:8080/delCon/1
${web_browser}              chrome
***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}
***Test Cases***
TC-PSF-03-13-01 ตรวจสอบการแสดงผลข้อมูลชื่อ - นามสกุล
    เปิดหน้าจอเว็บ