*** Settings ***
Library     SeleniumLibrary
Test Teardown       close Browser

***Variables***
${URL_page}                 http://127.0.0.1:8080/manageStudentCon_add
${web_browser}              chrome

${txt_ID}                   studentIDCon
${txt_name}                 nameStuCon
${txt_grade}                stuGpaCon
${txt_creditPass}           creditPassedCon
${txt_creditDown}           creditDownCon

${ID_1}                     64160000
${ID_2}                     66160123
${name_1}                   จิรายุ เจริญผลวัฒนา
${name_2}                   ไตรทศ พงศ์พัฒนา
${grade_1}                  3.50
${grade_2}                  2.50
${creditPass_1}             0
${creditPass_2}             125
${creditDown_1}             18
${creditDown_2}             125
${null}

${btn_summit}               ok

***Keywords***
เปิดหน้าจอเว็บ
    [Documentation]         ใช้เปิดหน้าจอ
    open browser            ${URL_page}         ${web_browser}

กรอกข้อมูล
    [Arguments]             ${txt_name}         ${txt}
    [Documentation]         ใช้สำหรับการกรอกข้อมูล
    input text              ${txt_name}         ${txt}

กรอกข้อมูลทั้งหมด
    [Arguments]             ${ID}       ${name}     ${grade}    ${creditPass}       ${creditDown}        
    [Documentation]         ใช้กรอกข้อมูลทั้งหมด   
    input text                      ${txt_id}               ${ID}
    input text                      ${txt_name}             ${name}
    input text                      ${txt_grade}            ${grade}
    input text                      ${txt_creditPass}       ${creditPass}
    input text                      ${txt_creditDown}       ${creditDown}

กดปุ่ม
    [Documentation]         ใช้กดปุ่มบันทึก
    click button            ${btn_summit}

กรอกข้อมูลว่าง
    [Arguments]             ${txt_name}
    [Documentation]         ใช้กดปุ่มบันทึก
    input text              ${txt_name}             ${null}

***Test Cases***
TS-PSF-03-04-01 ตรวจสอบการกรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_ID}               ${ID_1}

TS-PSF-03-04-02 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_name}             ${name_1}

TS-PSF-03-04-03 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_grade}            ${grade_1}

TS-PSF-03-04-04 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_creditPass}       ${creditPass_1}

TS-PSF-03-04-05 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_creditDown}       ${creditDown_1}

TS-PSF-03-04-06 ตรวจสอบการกรอกข้อมูลรหัสนิสิต รับไม่เกิน 8 ตัวอักษร และไม่น้อยกว่า 8 ตัวอักษร
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_ID}               ${ID_2}

TS-PSF-03-04-07 ตรวจสอบการกรอกข้อมูลชื่อ-นามสกุล รับไม่เกิน 50 ตัวอักษร
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_name}             ${name_2}

TS-PSF-03-04-08 ตรวจสอบการกรอกข้อมูลเกรดเฉลี่ย รับทศนิยมไม่เกิน 2 หลัก และทศนิยมไม่น้อยกว่า 2 หลัก
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_grade}            ${grade_1}

TS-PSF-03-04-09 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ผ่าน รับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_creditPass}       ${creditPass_2}

TS-PSF-03-04-10 ตรวจสอบการกรอกข้อมูลหน่วยกิตที่ลง รับเลขไม่เกิน 3 หลัก
    เปิดหน้าจอเว็บ
    กรอกข้อมูล                ${txt_creditDown}       ${creditDown_2}

TS-PSF-03-04-11 ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลรหัสนิสิต
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด          ${ID_1}      ${name_1}       ${grade_1}      ${creditPass_1}     ${creditDown_1}
    กรอกข้อมูลว่าง             ${txt_ID} 
    กดปุ่ม             

TS-PSF-03-04-12 ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลชื่อ-นามสกุล
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด          ${ID_1}      ${name_1}       ${grade_1}      ${creditPass_1}     ${creditDown_1}
    กรอกข้อมูลว่าง             ${txt_name} 
    กดปุ่ม
	
TS-PSF-03-04-13 ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลเกรดเฉลี่ย
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด          ${ID_1}      ${name_1}       ${grade_1}      ${creditPass_1}     ${creditDown_1}
    กรอกข้อมูลว่าง             ${txt_grade} 
    กดปุ่ม

TS-PSF-03-04-14 ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลหน่วยกิตที่ผ่าน
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด          ${ID_1}      ${name_1}       ${grade_1}      ${creditPass_1}     ${creditDown_1}
    กรอกข้อมูลว่าง             ${txt_creditPass} 
    กดปุ่ม

TS-PSF-03-04-15 ตรวจสอบการกรอกข้อมูลหากไม่กรอกข้อมูลหน่วยกิตที่ลง
    เปิดหน้าจอเว็บ
    กรอกข้อมูลทั้งหมด          ${ID_1}      ${name_1}       ${grade_1}      ${creditPass_1}     ${creditDown_1}
    กรอกข้อมูลว่าง             ${txt_creditDown} 
    กดปุ่ม
